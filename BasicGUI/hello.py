from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys

xpos = 200
ypos = 200
width = 300
height = 100


def window():
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setGeometry(xpos, ypos, width, height)
    win.setWindowTitle("Lucas Muniz fez isso")

    label2 = QtWidgets.QLabel(win)
    label2.setText("Olá mundo")
    label2.move(20, 40)

    label = QtWidgets.QLabel(win)
    label.setText("Hello world")
    label.move(20, 50)

    win.show()
    sys.exit(app.exec_())


window()
