from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys


class MyWin(QMainWindow):
    def __init__(self):
        super(MyWin, self).__init__()
        self.setGeometry(200, 300, 200, 200)
        self.setWindowTitle("button")
        self.initUI()

    def initUI(self):
        self.label = QtWidgets.QLabel(self)
        self.label.setText("Hello world")
        self.label.move(20, 50)

        self.b1 = QtWidgets.QPushButton(self)
        self.b1.setText("Click me")
        self.b1.clicked.connect(self.Clicked)

    def Clicked(self):
        self.label.setText("you pressed the button")
        self.update()

    def update(self):
        self.label.adjustSize()


def window():
    app = QApplication(sys.argv)
    win = MyWin()

    win.show()
    sys.exit(app.exec_())


window()
